//
//  PhotoGalleryApp.swift
//  PhotoGallery
//
//  Created by Frank Marx on 10.01.22.
//

import SwiftUI

import Logging

fileprivate let log = Log.logger()

@main
struct PhotoGalleryApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
