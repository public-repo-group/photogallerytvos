
import SwiftyBeaver

#if DEBUG
let console: BaseDestination = {
        let con = ConsoleDestination()
        con.format = "$DHH:mm:ss$d $n: $T $F:$l $L: $M"
        con.minLevel = .info
        return con
    }()
#else
    let console: BaseDestination = {
        let con = ConsoleDestination()
        con.format = "$DHH:mm:ss$d $n: $T $F:$l $L: $M"
        con.minLevel = .error
        return con
    }()
#endif

internal let log: SwiftyBeaver.Type = {
    let l = SwiftyBeaver.self
    l.addDestination(console)
    return l
}()

public final class Log {

    internal static let log: SwiftyBeaver.Type = {
        let l = SwiftyBeaver.self
        l.addDestination(console)
        return l
    }()

    public static func logger() -> SwiftyBeaver.Type {
        log
    }

}

